package com.src.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MainServiceTest {

	HotelBooking hb;

	public MainServiceTest() {

		hb = new HotelBooking();
	}

	@Test
	public void testBookHotelRoom() {
		int roomBooked = hb.bookHotelRoom(0, 5);
		assertEquals(1, roomBooked);

	}

	@Test
	public void testBookHotelRoomNegative() {
		int roomBooked = hb.bookHotelRoom(0, 365);
		assertEquals(0, roomBooked);

	}

	@Test
	public void testBookHotelRoomNegative2() {
		int roomBooked = hb.bookHotelRoom(-1, 5);
		assertEquals(0, roomBooked);

	}

}
