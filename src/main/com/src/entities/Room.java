package com.src.entities;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Room {
	
	private String name;
	private List<Booking> bookingList;
	private Set<Integer> availabilty;
	
	public Room(String name) {
		this.name = name;
		Set<Integer> set = new HashSet<Integer>();
		for(int i = 0;i<365 ; i++) {
			set.add(i);
			
		}
		availabilty  = set;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean getAvailability(List<String> list) {
		return true;
	}
	
	public List<Booking> getBookingList() {
		return bookingList;
	}
	public void setBookingList(List<Booking> bookingList) {
		this.bookingList = bookingList;
	}
	public Set<Integer> getAvailabilty() {
		return availabilty;
	}
	public void setAvailabilty(Set<Integer> availabilty) {
		this.availabilty = availabilty;
	}
	
	

}
