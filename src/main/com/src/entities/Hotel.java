package com.src.entities;

import java.util.ArrayList;
import java.util.List;

public class Hotel {
	
	private int numberOfRoom;
	private List<Room> roomList ;

	public int getNumberOfRoom() {
		return numberOfRoom;
	}

	public void setNumberOfRoom(int numberOfRoom) {
		this.numberOfRoom = numberOfRoom;
		roomList = new ArrayList<Room>();
		for(int i = 1; i<= numberOfRoom ; i++) {
			Room room = new Room("Room" + i);
			roomList.add(room);
		}
	}

	public List<Room> getRoomList() {
		return roomList;
	}

	public void setRoomList(List<Room> roomList) {
		this.roomList = roomList;
	}
	

	
	

}
