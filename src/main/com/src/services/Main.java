package com.src.services;

public class Main {

	public static void main(String[] args) {

		if (args.length == 0) {
			System.out.println("no argument provided so taking an example ");
			String[] arg = new String[] { "0,5", "2,3", "4,5" };
			args = arg;
		}

		for (String input : args) {
			String[] dateArr = input.split(",");
			if (dateArr.length != 2) {
				System.out.println(
						"Enter argument for 1 booking separated by ',' and for multiple bookings separated by <space> e.g.- 0,5 4,6");
			} else {
				HotelBooking hb = new HotelBooking();
				hb.bookHotelRoom(Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1]));

			}
		}

	}

}
