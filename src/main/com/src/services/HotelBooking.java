package com.src.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import com.src.entities.Hotel;
import com.src.entities.Room;

public class HotelBooking {

	static Hotel myHotel;

	static {

		try (InputStream input = ClassLoader.getSystemClassLoader().getResourceAsStream("prop.properties")) {

            Properties prop = new Properties();

            if (input == null) {
                System.out.println("Unable to find prop.properties");
                
            }
		

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			System.out.println("Hotel Total Rooms available - "+prop.getProperty("numberOfRoom"));
			myHotel = new Hotel();
			myHotel.setNumberOfRoom(Integer.parseInt(prop.getProperty("numberOfRoom")));

		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	private boolean isBookingDateValid(int start, int end) {
		boolean isValid = true;

		if (start < 0 || start > 364) {
			System.out.println("Start date is not valid -> "+start);
			isValid = false;
		} else if (end < 0 || end > 364) {
			System.out.println("End date is not valid -> "+end);
			isValid = false;
		} else if (end < start) {
			System.out.println("Date range is not valid -> "+start+","+end);
			isValid = false;
		}

		return isValid;
	}

	private Room getFreeRoom(Set<Integer> dateRange) {

		for (Room hotelRoom : myHotel.getRoomList()) {
			if (hotelRoom.getAvailabilty().containsAll(dateRange)) {
				return hotelRoom;

			}
		}
		return null;
	}

	private Set<Integer> getDateRange(int start, int end) {
		Set<Integer> dateRangeList = new HashSet<Integer>();
		for (int i = start; i <= end; i++) {
			dateRangeList.add(i);
		}
		return dateRangeList;
	}

	private void bookRoom(Room freeRoom, Set<Integer> dateRange) {

		freeRoom.getAvailabilty().removeAll(dateRange);
		//System.out.println("RoomBooked - " + freeRoom.getName());

	}

	public int bookHotelRoom(int start, int end) {
		int roomBooked = 0;
		String retStr;
		if (isBookingDateValid(start, end)) {
			Set<Integer> dateRange = getDateRange(start, end);
			Room freeRoom = getFreeRoom(dateRange);
			if (null == freeRoom) {
				roomBooked = 0;
				retStr = "No Room available on given dates - "+start+","+end;

			} else {
				bookRoom(freeRoom, dateRange);
				roomBooked = 1;
				retStr = "Room Booked for given dates - " + start +"," + end +"- "+freeRoom.getName();
			}
		} else {
			roomBooked = 0;
			retStr = "Enter valid dates for booking -> "+start+","+end;
		}
		
		System.out.println(retStr);
		return roomBooked;
	}

}
